package com.example.cse438.studio1.activity

import android.app.ActionBar
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.Toast
import com.example.cse438.studio1.R
import com.example.cse438.studio1.fragment.HomeFragment
import com.example.cse438.studio1.fragment.ResultListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    enum class Fragments {
        HOME,
        RESULTS
    }
    private var currentView: Fragments = Fragments.HOME

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_home)

        // Load Fragment into View
        val fm = supportFragmentManager

        // add
        val ft = fm.beginTransaction()
        ft.add(R.id.frag_placeholder, HomeFragment(this))
        ft.commit()

        //Examine the search functionality below
        search_edit_text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searchText = search_edit_text.text
                search_edit_text.setText("")
                if (searchText.toString() == "") {
                    val toast = Toast.makeText(this@MainActivity, "Please enter text", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                    return@setOnEditorActionListener true
                }
                else {
                    performSearch(searchText.toString())
                    return@setOnEditorActionListener false
                }
            }

            return@setOnEditorActionListener false
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//Top right menu icon and options


//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        when (item.itemId) {
//            R.id.action_settings -> return true
//            else -> return super.onOptionsItemSelected(item)
//        }
//    }

    // TODO: Modify the function below to use the new navigation options and add code to handle them
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                if(currentView != Fragments.HOME){
                    // Load Fragment into View
                    val fm = supportFragmentManager

                    // add
                    val ft = fm.beginTransaction()
                    ft.remove(fm.findFragmentById((R.id.frag_placeholder))!!)
                    ft.add(R.id.frag_placeholder, HomeFragment(this@MainActivity))
                    ft.commit()
                }

            }
            R.id.nav_aboutUs -> {
                displayDialog(R.layout.dialog_about_us)

            }
            R.id.nav_privacyPolicy -> {
                displayDialog(R.layout.dialog_privacy_policy)

            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    // TODO: Implement this function to empty the existing Fragment slot and attach the ResultListFragment
    private fun performSearch(query: String) {

        // Load Fragment into View
        val fm = supportFragmentManager

        // add
        val ft = fm.beginTransaction()
        ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
        ft.add(R.id.frag_placeholder, ResultListFragment(this@MainActivity, query))
        ft.commit()

        currentView = Fragments.RESULTS

    }

    private fun displayDialog(layout: Int) {
        val dialog = Dialog(this)
        dialog.setContentView(layout)

        val window = dialog.window
        window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        dialog.findViewById<Button>(R.id.close).setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}
